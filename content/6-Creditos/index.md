# Créditos

### Autoría

sta guía ha sido elaborada por *Ondula (@ondula)*, una asociación sin ánimo de lucro dedicada a la educación digital y en tecnologías orientada al desarrollo personal a través del pensamiento crítico, creativo y ético, la implementación de metodologías activas de aprendizaje basadas en la experiencia, la cooperación y la práctica artística, y la innovación educativa orientada a la transformación social por un mundo más justo. Queremos compartir no sólo nuestra visión y experiencia de las posibilidades del aprendizaje de la programación de código informático, sino también la vivencia de crear y formar parte de una comunidad de aprendizaje como es Coder Dojo, y el potencial de colaboración, reflexión y acción social que hace un uso consciente y trasciende la tecnología poniéndola al servicio de las personas.

### Licencia de uso

Las guías didácticas de La Aventura de Aprender están publicadas bajo la siguiente licencia de uso Creative Commons: CC-BY-SA 3.0. Reconocimiento – CompartirIgual (by-sa): que permite compartir, copiar y redistribuir el material en cualquier medio o formato, así como adaptar, remezclar, transformar y crear a partir del material, siempre que se reconozca la autoría del mismo y se utilice la misma licencia de uso.
