#Introducción

Narrativas digitales, animaciones, videojuegos, aplicaciones… todo eso y mucho más de lo que utilizas en tu día a día ha sido programado por alguien. Aprendemos a usarlos con facilidad, pero usar, ver y consumir no es todo. Tal vez hayas pensado que hay cosas que se pueden mejorar, que es posible diseñar aplicaciones (apps) que sean útiles a personas de tu entorno o que puedes contar una historia distinta mediante un videojuego.

Bien, pues esto es lo que podrás hacer en Coder Dojo: aprender a programar desde sencillos lenguajes informáticos de programación por bloques. Pero además, Coder Dojo es un espacio de aprendizaje compartido y protagonizado por un grupo de jóvenes donde podemos llevar adelante proyectos en común.

¿Te has planteado alguna vez que aprender a programar se puede hacer jugando? En Coder Dojo, aprender es una experiencia alegre: no hay libros ni exámenes, y nosotros marcamos el ritmo:

- investigamos por nuestra cuenta,
- conocemos los elementos y trucos de la programación, resolvemos retos juntos, probamos cosas y las mejoramos,
- creamos nuestros propios relatos y también herramientas útiles para nuestra comunidad.

Esta forma de aprender permite desarrollar el pensamiento computacional y la creatividad, mientras aprendemos de forma colaborativa.

En Ondula creemos que aprender a pensar de manera creativa y lógica y aprender de nuestros errores es además importante para otras facetas de la vida: para abordar problemas y conflictos del día a día, para organizarnos colectivamente, para planificar el estudio, poner en marcha un proyecto o movilizarnos por una causa justa. Cuando además somos capaces de conectar estos aprendizajes con una sensibilidad por nuestro entorno, un análisis de la realidad (relaciones personales, familia, escuela, barrio) y hacer proyectos para dar respuesta a esta realidad, entonces un Coder Dojo es un espacio para crecer como personas y para contribuir a la comunidad.

En Coder Dojo importa tanto lo que ya sabes, como lo que estás dispuesto a aprender. Si eres estudiante, profesor, padre o madre, puedes poner en marcha un Coder Dojo: cualquier persona puede. En esta guía te daremos algunas pautas para empezar.

![](images/CoderDojo-sesion-Irlanda.jpg)


## ¿Por qué este nombre?
Coder Dojo es una iniciativa internacional que surgió en Irlanda en 2011 para enseñar a niñas, niños y jóvenes a programar. Se trata de una comunidad de aprendizaje informal, en la que un grupo de jóvenes empieza y desarrolla sus propios programas con apoyo de personas adultas voluntarias: mentores y mentoras. La Fundación Coder Dojo ubicada en Dublín estima que existen ya más de 1.250 clubes en 69 países, y anima a nuevas iniciativas a unirse a la red.

El nombre viene inspirado por el concepto dojo: un espacio destinado a la práctica y enseñanza de la meditación y otras artes marciales tradicionales en Japón. En un dojo, los jóvenes aprenden y practican con la supervisión y apoyo de un 'maestro'. En Coder Dojo se mantienen estas ideas de la importancia del espacio y el aprendizaje práctico de los jóvenes con la supervisión y apoyo de mentores y mentoras, que aportan su experiencia y conocimiento de programación o de dinamización de actividades educativas con jóvenes.

## ¿En qué consiste aprender a programar?
La programación consiste en escribir líneas de código informático que contienen las instrucciones para que una máquina (ordenador, móvil o tablet) realice unas tareas determinadas. El código informático o lenguaje de programación es un lenguaje que la máquina 'entiende', como por ejemplo Java o Python, y es parecido al lenguaje matemático. Al igual que las matemáticas, la programación comprende una lógica que se define con un algoritmo: una manera de ordenar instrucciones para realizar algo.

Un ejemplo cotidiano de un algoritmo es una receta de cocina, que contiene los ingredientes y los pasos necesarios para cocinar con ellos un rico plato. En Coder Dojo empezaremos utilizando entornos de programación visual por bloques, para hacer más sencillo y accesible este aprendizaje. ¿Qué vamos a programar? ¿Para qué lo hacemos? Éste es el quid de la cuestión.

Imagina que quieres programar un pequeño videojuego que cuenta una historia interactiva:

- lo primero es pensar qué queremos contar, o más bien, qué es necesario contar,
- después empezar a imaginar y crear una historia,
- desarrollar los personajes involucrados,
- describir qué sucede y cómo se interactúa con el videojuego.

Pensar para qué hacemos las cosas tiene que ver con el propósito, con que lo que hagamos tenga sentido. Podemos crear historias que tienen que ver con lo que conocemos, con nuestra experiencia, con nuestro entorno. Si empezamos a crear aplicaciones para móviles, nuestra tarea cobra mayor sentido cuando nos planteamos qué necesidades vemos en nuestra realidad y cómo podemos aportar a resolver un determinado problema real. ¿Cómo lo haremos? Prueba y error. Aprendemos haciendo, y vamos dando pasos poco a poco. Hay muchos elementos involucrados en la programación de un videojuego:

- aspectos técnicos: conocer las instrucciones, editar imágenes o elaborar el sonido,
- y aspectos no técnicos: construir relatos, diseñar los retos y cooperar para abordarlos juntos.

En cada paso, probamos y comprobamos, aprendiendo de nuestros errores. Además, hay cosas que haremos en el ordenador y cosas que haremos fuera del ordenador, mediante dinámicas unplugged (desconectadas):

- comprender conceptos,
- crear relatos y dibujarlos en un storyboard,
- resolver problemas juntos son actividades que podemos hacer con rotuladores y papel.

Hay momentos para trabajar individualmente y en grupo, intercambiando ideas, diseñando y reflexionando. Esto es parte del aprendizaje cooperativo: somos una comunidad de aprendizaje y podemos aprender más y mejor juntos que cada uno por separado.

![](images/Aprendizaje-Cooperativo.jpg)

## Algunos conceptos clave
- Software: Un programa informático.
- Software privativo: Tiene una licencia de uso que no permite conocer o modificar su código, como Microsoft - Windows o Facebook.
- Software libre: Tiene una licencia abierta que permite usar, conocer, modificar y distribuir su código, como Linux o Mozilla Firefox.
- Programación por bloques: Selecciona en pantalla bloques de código ya escritos, para modificarlos y ordenarlos de manera sencilla.
- Scratch: Entorno de programación visual por bloques para crear animaciones y sencillos videojuegos.
- App Inventor: Versión de Scratch para programar aplicaciones en ordenadores y móviles con Android.
- Innovación educativa: Nuevas formas de aprender conceptos, practicar, reflexionar, colaborar y ser creativos.
- Entornos cooperativos de aprendizaje: Favorecen aprender juntos, complementando los saberes y experiencia de los participantes.
- Aprender haciendo: Empezamos por la práctica, no por la teoría: prueba y error. La experiencia es el detonante para el aprendizaje.
- DIY-DIWO: Del inglés Do-It-Yourself (que significa 'hazlo tú mismo') y Do-It-With-Others ('hazlo con otros').
- Dinámicas 'Unplugged': Actividades que se pueden desarrollar fuera del uso de ordenadores para comprender conceptos informáticos.
- Storyboard: Plantilla de diseño de historias en formato de viñetas de cómic.

##¿Qué nos va a aportar?
*«Me lo contaron y lo olvidé, lo vi y lo entendí, lo hice y lo aprendí» Confucio, siglo VI a.C.*

![](images/Konfuzius-1770.jpg)

**Experiencia**

Poner en marcha un proyecto y participar de una comunidad de aprendizaje son experiencias apasionantes, que además podremos llevar a otros ámbitos de nuestra vida.

Cuando hablamos de aprender haciendo, ese 'hacer' es programar, pero también organizar, comunicar y todo lo que constituye esa experiencia: pasión, curiosidad, emociones, ideas, hechos, diálogos, reflexiones...

**Aprender a aprender**

Es una de las claves para el aprendizaje a lo largo de la vida. No sólo aprendemos en la escuela o en la universidad. Aprendemos fundamentalmente de nuestras experiencias más directas: de lo que hacemos, de lo que nos pasa, incluso de las dificultades. Ésta es una de las competencias clave que la Comisión Europea identifica en el siglo XXI.

**Colaborar y cooperar**

La forma en que trabajamos ha cambiado: existen múltiples experiencias que demuestran que la colaboración aporta mejores resultados en la consolidación del conocimiento y diferentes perspectivas en la resolución de problemas.

La colaboración es el intercambio de saberes y experiencias, y la cooperación es un paso más: trabajamos juntos para lograr un objetivo común. Aprender de manera colaborativa y cooperativa nos refuerza para el trabajo en equipo, desarrollando habilidades de escucha, empatía y diálogo con personas diversas.

**Planificar y organizar**

Coder Dojo implica dar continuidad a una actividad educativa dentro de una comunidad. Nos permitirá desarrollar habilidades para la planificación de las sesiones, proyectos y otros eventos, y la organización de la comunidad y de la actividad.

**Conocimiento, habilidades de pensamiento y comunicación**

Adquirimos conocimientos de programación y de los distintos temas sobre los que traten nuestros proyectos. Al mismo tiempo, aprendemos a pensar mejor: el pensamiento es crítico, creativo y ético, y lo ponemos en práctica al resolver problemas.

Los productos que generamos en el Coder Dojo (animaciones, videojuegos, aplicaciones...) son una forma de comunicar al mundo, y mientras los realizamos de forma colaborativa aprendemos a expresarnos y comunicarnos.

![](images/coderdojo-girls.jpg)

## ¿Por qué ahora?
Diversos autores reivindican en los últimos años que saber programar es una de las habilidades necesarias para la ciudadanía en el siglo XXI. Mucho de lo que utilizamos en nuestro día a día contiene software.

En la medida en que somos meros usuarios o consumidores de aplicaciones o productos audiovisuales, y no entendemos cómo están hechos los algoritmos que hay detrás, somos, en cierto modo, analfabetos.

Empezar a construir nuevas lógicas y pensar cuál es la lógica detrás de las aplicaciones que usamos, es una oportunidad de re-educarnos para ser más libres.

*«Una sociedad libre necesita personas libres» Richard Stallman, Presidente de la Fundación para el Software Libre (FSF)*

En los últimos años han surgido múltiples iniciativas de innovación educativa que abordan nuevas formas de aprender, nuevas metodologías y enfoques del aprendizaje que trascienden la manera en que, en general, se sigue enseñando en la escuela. Estas iniciativas centran el aprendizaje en la experiencia (individual y colectiva) en el juego, en la emoción y las artes, en abordar problemas reales y no teóricos, sacar adelante retos y proyectos. Hoy, aprender a programar necesita ser una experiencia alineada con estas tendencias.

La cultura colaborativa también ha tomado fuerza en los últimos años, facilitada por herramientas digitales de intercambio en la red. Iniciativas que suceden en un lugar forman parte de una comunidad mucho más amplia en todo el mundo: es el ejemplo de las comunidades de software libre o los Coder Dojos.

A su vez, desde una perspectiva de participación ciudadana, nos empoderamos conjuntamente al poner en marcha espacios auto-gestionados de aprendizaje y conocimiento. Tomamos un papel activo en la Sociedad de la Información.
