# Recursos
- Sitio oficial de la comunidad y la Fundación Coder Dojo.
- Coder Dojo ECHO: principios y valores. Enlace al PDF.
- Ser mentor/a. Enlace al PDF.
- Actividades unplugged (desconectadas).
- Guía de Alfabetización Digital Crítica: una invitación a reflexionar y actuar. Enlace al PDF.

## Programas
- Scratch: Proyectos, recursos, tutoriales y comunidad de usuarios/as.
- Guía de Informática creativa con Scratch: Introducción de conceptos, planificación y metodología de sesiones. Enlace al PDF.
- App Inventor: Proyectos, recursos, tutoriales y comunidad.
- Guía de iniciación a App Inventor. Enlace al PDF.
