
#Cómo hacer un Coder Dojo

![](images/images.jpg)

En la guía LADA sobre **'Cómo hacer un Coder Dojo'** veremos cómo podemos montar un espacio de aprendizaje compartido y protagonizado por un grupo de jóvenes donde podemos llevar adelante proyectos en común vinculados con la programación, y donde aprenderemos a programar desde sencillos lenguajes informáticos de programación por bloques.

Esta guía ha sido elaborada por *Ondula (@ondula)*, una asociación sin ánimo de lucro dedicada a la educación digital y en tecnologías orientada al desarrollo personal a través del pensamiento crítico, creativo y ético, la implementación de metodologías activas de aprendizaje basadas en la experiencia, la cooperación y la práctica artística, y la innovación educativa orientada a la transformación social por un mundo más justo. Queremos compartir no sólo nuestra visión y experiencia de las posibilidades del aprendizaje de la programación de código informático, sino también la vivencia de crear y formar parte de una comunidad de aprendizaje como es Coder Dojo, y el potencial de colaboración, reflexión y acción social que hace un uso consciente y trasciende la tecnología poniéndola al servicio de las personas.
