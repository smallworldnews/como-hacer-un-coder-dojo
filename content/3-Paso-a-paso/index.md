# Paso a paso

Si estás leyendo esta guía ya habrás podido imaginar cómo será contar con un Coder Dojo en tu barrio, ya sea en tu colegio o instituto, en el centro cultural o la biblioteca, o en el local de una asociación. Ahora lo importante es empezar, pues no basta con tener buenas o grandes ideas, sino ¡llevarlas a cabo! Vamos por partes.

## Paso CERO: Conoce Scratch, una herramienta básica

Hemos visto que una de las herramientas con las que se suele empezar el aprendizaje de la programación es Scratch: una plataforma de software libre para la programación visual por bloques diseñada por el Media Lab del Instituto Tecnológico de Massachusetts (MIT). Así que vamos a ver un poquito sobre Scratch como un ejemplo práctico y sencillo para empezar.

Scratch está diseñado especialmente para edades entre los 8 y 16 años, pero lo utilizan personas de todas las edades, y está disponible en español. El entorno visual que proporciona Scratch nos permite programar de manera similar a montar un puzzle:

- vamos tomando bloques de código y los añadimos al proyecto,
- probamos cómo funciona,
- luego podemos cambiar el orden, quitar o añadir nuevos bloques.

Estos bloques permiten diversas funciones para animar objetos y personajes en pantalla tales como mover y girar, cambiar la apariencia (forma, tamaño, color…) del objeto o el fondo, hacer sonar pistas de audio, añadir condiciones y bucles, crear variables, etc.


## Empieza a utilizar Scratch

En la página de Scratch del MIT puedes:

1. Explorar y conocer proyectos ya creados y publicados, para conocer algunos ejemplos y posibilidades de uso que nos pueden inspirar. Dado que Scratch se basa en la filosofía del software libre, los proyectos se comparten también en código abierto, es decir: puedes ver el resultado final de una historia o un videojuego, y también puedes ver el código que está programado, modificarlo o tomar una pieza de ese código para uno de tus proyectos.

2. Probar Scratch online: conocer cómo es el interfaz de la plataforma y empezar a programar online, o bien descargar el Editor sin conexión de Scratch 2.0 en tu ordenador personal, ya que dispone de versiones instalables en Linux, Mac y Windows. Si quieres utilizar Scratch de forma continuada, por ejemplo en el Coder Dojo, ésta es la opción más recomendable por agilidad de uso e independencia de la conexión a Internet.

3. Empezar probando con algunos ejercicios sencillos. En la sección de Sugerencias encontrarás breves actividades para principiantes como animar un nombre, mover un personaje, crear una historia o un videojuego sencillo como el pong. Para cada actividad se aporta:
- Un tutorial para realizar el proyecto en Scratch paso a paso.
- Tarjetas de actividad con pequeños bloques de código que puedes imprimir y utilizar como material didáctico en Coder Dojo.
- Guía para educadores con indicaciones para llevar a cabo un taller de Scratch de 1 hora.

## ¡Sigue investigando!

Hay multitud de tutoriales, vídeos, artículos y guías sobre cómo programar con Scratch y cómo dinamizar actividades educativas. Encontrarás lugares como la web de Scratch para educadores con formas de aprender sobre diversas materias (arte, ciencias sociales, filosofía o historia) o sobre temáticas sociales (consumo, medioambiente, diversidad, etc.).

También puedes seguir investigando por tu cuenta sobre otras plataformas o lenguajes de programación como Minetest, App Inventor o Processing, o dispositivos electrónicos programables como Arduino o Raspberry Pi. Hay mucho por descubrir y por hacer. Pero no lo hagas todo ahora, ¡crea un Coder Dojo para compartir este camino!

Web de Scratch para educadores

## 1. Hacer equipo
*«Solo no puedes, con amigos sí» La bola de cristal*

**¿Tienes ganas de montar un Coder Dojo? Una de las mayores aportaciones de Coder Dojo es que podemos programar juntos, y al mismo tiempo aprender, compartir nuestros conocimientos y experiencia, colaborar. Estos planteamientos han de estar presentes desde el principio y verás como todo es diferente.**

Empieza por compartir lo que ahora sabes sobre Coder Dojo con otras personas de tu entorno (amigos y amigas, familia, compañeros y compañeras de clase o del trabajo, vecinos y vecinas): encuentra lo que te apasiona de esta idea y háblales de lo que te gustaría hacer, de por qué es importante aprender a programar, de cómo puede ser aprender juntos y dinamizar vuestro propio espacio y crear una comunidad de aprendizaje.

Cuando montamos un nuevo proyecto, son muchas las personas a las que hablaremos del proyecto y las invitaremos a participar. Nos responderán con diversas ideas y opiniones que pueden ayudarnos a reflexionar, a saber cómo abordarlo, a encontrar errores en nuestro planteamiento y mejorarlo. Muchas nos apoyarán de diversas maneras, y sólo algunas querrán subirse a este barco. Para empezar bastará con reunir un pequeño grupo de 2 ó 3 personas que quieran formar parte de un equipo motor para arrancar el proyecto.

El equipo motor nace para cooperar: dialogamos, compartimos lo que sabemos, nos coordinamos para realizar tareas. Hay tres pilares fundamentales en la construcción de un equipo cohesionado y preparado para cooperar en el arranque de un proyecto:

- Pasión: ¡Sentir pasión por el proyecto! La pasión es el motor para la acción, que nos permite seguir adelante a pesar de las dificultades que encontraremos en el camino.
- Confianza: Entre nosotros/as, en nuestras capacidades y en los objetivos que perseguimos. La confianza se construye desde el cuidado, la escucha y el diálogo.
- Compromiso: Tomar decisiones implica libertad y responsabilidad para llevarlas a cabo. Al cumplir nuestros compromisos reforzamos la confianza en el equipo.

Una idea para empezar: ¡Visitar un Coder Dojo existente en vuestra ciudad! Esto os permitirá dar los primeros pasos juntos: buscar y contactar con otros Coder Dojos, planificar la visita, tomar nota y compartir ideas que os surgen de lo que habéis visto, preguntar a otros cómo empezaron y qué os pueden recomendar.

**Comunicación interna: coordinación**

Toda buena organización requiere una comunicación abierta, clara y fluida. Para organizarnos internamente un punto clave son las reuniones presenciales, especialmente al inicio del proyecto ya que estamos aprendiendo a trabajar juntos: nos permiten aclarar puntos de vista, resolver conflictos, etc.

También podemos empezar a utilizar alguna herramienta de comunicación digital como un grupo de Telegram (software libre) o WhatsApp (software privativo), para cuestiones más urgentes, o el correo electrónico para desarrollar algunos temas que requieren más información o detalle. Fíjate que en este momento de arranque del proyecto somos pocas personas, pero más adelante los momentos de coordinación presencial o digital se abrirán a todas las personas que formen parte de nuestra comunidad de Coder Dojo.

Por ejemplo, será posible hacer reuniones de coordinación antes o después de las sesiones de aprendizaje (incluyendo mentores y familias) y podremos crear una lista de correo gratuita en GNU Mailman (software libre) o en Google Groups (software privativo), donde añadir nuevos miembros del proyecto.

## 2. Conseguir un espacio
El primer reto de vuestro nuevo equipo es conseguir un espacio para Coder Dojo en vuestro barrio. ¿Has echado un vistazo a los espacios que hay en tu entorno, en tu barrio? Un Coder Dojo se puede montar incluso en una casa, aunque si tenemos éxito es probable que estemos hablando de un espacio que pueda albergar inicialmente a unas 15-20 personas.

Hay Coder Dojos que suceden en aulas de un colegio o instituto, en la biblioteca del barrio, en un centro cultural o cívico de carácter público o en un centro comunitario, en locales de asociaciones culturales o de vecinos y vecinas. Éste es un momento para mirar al barrio y preguntarnos de qué espacios disponemos para realizar actividades promovidas desde las vecinas y vecinos.

Puede que ya existan iniciativas ciudadanas que también reivindican y dan uso a espacios públicos o comunes, como por ejemplo un huerto urbano o un grupo de auto-aprendizaje de yoga, cocina o de ganchillo. Entonces lo tendremos más fácil porque podremos preguntar a estos colectivos y aprender de su experiencia, incluso compartir ese mismo espacio en otros horarios. Si no es así, toca revisar los espacios que hay en el barrio, ver qué uso tienen y solicitar hacer un uso colectivo (ciudadano) de los mismos. Esta perspectiva es apasionante pues estamos creando nuevos espacios, reapropiándonos y resignificando nuestro entorno.

A la hora de buscar y solicitar el uso de un espacio deberemos tener en cuenta:

**Lo que necesitamos de un espacio**
- Infraestructura y aforo disponibles.
- Espacio inclusivo.
- Horario y calendario.

**Lo que hay que saber sobre un espacio**
- Propiedad o titularidad.
- Normas de uso y convivencia.
- Responsabilidad civil.

![](images/coderdojo-medialabprado6.jpg)



### Lo que necesitamos
Estas son cuestiones que debemos recopilar, puede ser en la forma de una breve lista de requisitos, antes de acercarnos a hablar con las personas que gestionan los diversos espacios en el barrio. ¿Qué necesitamos de un espacio donde realizar un Coder Dojo?

**Infraestructura y aforo**
Tal y como hemos visto en el apartado de Materiales, el espacio deberá disponer de una infraestructura básica: mesas y sillas móviles que permitan la programación en ordenadores portátiles, zonas comunes para jugar, dibujar y diseñar fuera del ordenador, red eléctrica con varios enchufes, y conexión a Internet. Para empezar, vale la pena pensar en que esta sala tenga un aforo de unas 20 personas, es decir, un tamaño suficiente para albergar esta cantidad de personas.

Adicionalmente, podemos consultar la existencia o disponibilidad de uso de infraestructura adicional en la sala, como por ejemplo pizarra o papelógrafo, proyector, altavoces, regletas de enchufes. Si el espacio dispone también de estos materiales nos facilita mucho las cosas pero, si no es así, tampoco es un problema: son cosas que podemos conseguir inicialmente desde el equipo motor y que posteriormente pueden ser parte de los materiales comunes de la comunidad.

A veces, en Coder Dojo cada participante trae su portátil para trabajar en la sesión, pero en ocasiones esto no es posible y deberemos facilitar que haya ordenadores o portátiles disponibles en la sala, ya sean proporcionados por el propio espacio o bien podremos conseguirlos mediante donaciones o acuerdos con otras entidades.

**Espacio inclusivo**
Coder Dojo es una actividad sin ánimo de lucro, que se desarrolla gracias al trabajo voluntario de todos los miembros de la comunidad (equipo motor, mentores y mentoras, participantes, familias). Cualquier persona puede participar de manera gratuita, y esto ayuda increíblemente a que Coder Dojo sea un espacio inclusivo y accesible a cualquiera. Por tanto, también el espacio que utilicemos deberá ser de uso gratuito, sin necesidad de pagar un alquiler o comprar una licencia, para evitar que alguien se pueda quedar fuera.

**Horario y calendario**
Los espacios no suelen estar vacíos hasta que nosotros llegamos, sino que ya están siendo utilizados. Así, podremos plantear que necesitamos un espacio con los requisitos mencionados durante determinado(s) días(s) y horas a la semana. Coder Dojo se realiza en horario extraescolar, de manera informal y con posibilidad de que las familias participen también de la comunidad.

Por ejemplo, los sábados en horario de 17h a 19h es cuando tiene lugar el Coder Dojo de Medialab-Prado, una de las iniciativas pioneras en España. Al mismo tiempo necesitaremos especificar cuál es la duración en meses de la actividad, que generalmente abarca casi todo el curso escolar: por ejemplo, de octubre a mayo. Esto no significa que solo se puede empezar un Coder Dojo en octubre, sino que podemos empezarlo en cualquier momento y mantener la actividad de forma continuada hasta el verano. Eso sí, al año siguiente, empezaremos desde principio de curso.

![](images/coderdojo-medialabprado8.jpg)

### Lo que hay que saber sobre un espacio
Estas son las preguntas que deberemos hacernos y hacer a las personas responsables del espacio:

**¿De quién es este espacio?**
La propiedad o titularidad del espacio nos indica quiénes serán nuestros interlocutores, es decir, a qué personas nos deberemos dirigir para solicitar su uso. Podemos ir a la biblioteca del barrio, que puede ser pública (del ayuntamiento o la diputación provincial) o privada. Según sea el caso, habrá una persona coordinadora de actividades en la biblioteca y el procedimiento para solicitar su uso puede ser distinto (rellenar un formulario en papel u online, realizar un acuerdo).

Otras opciones bien interesantes para reforzar el tejido asociativo en el barrio pueden ser locales de asociaciones culturales o vecinales, en cuyo caso será necesario hablar con las personas coordinadoras y tomarán una decisión en Asamblea o bien con la Junta Directiva.

**¿Cómo aproximarnos?**
Es verdad que a día de hoy muchas entidades públicas o privadas que gestionan espacios pueden contactarse por teléfono, a través de la web o las redes sociales. Sin embargo, este método de aproximarnos a los espacios es muy poco exitoso. ¿Por qué? ¡Porque no nos conocen! Lo más fructífero es siempre acercarnos al espacio, contar a las personas que allí estén lo que queremos hacer, solicitar una reunión con la persona responsable de gestionar las actividades y encontrarnos con esta(s) persona(s) en vivo y en directo.

**¿Cómo debemos utilizarlo?**
Generalmente, existen unas normas de uso y convivencia del espacio que también la comunidad de Coder Dojo deberá conocer y respetar: por ejemplo, utilización, cuidado y devolución de materiales, espacios en silencio, uso de WiFi e internet, etc. Puede que haya otras formalidades necesarias, tales como rellenar una ficha de la actividad o realizar un registro de asistencia. Conocer y respetar estas normas de uso es un elemento clave para la convivencia entre la comunidad de Coder Dojo, el espacio y todas las personas involucradas. Es posible que adicionalmente haya otras condiciones o recursos que se pueden dialogar, compartir y resulten beneficiosas para ambas partes, como por ejemplo si el espacio dispone de un sitio web o en redes sociales, podemos proponer que la actividad de Coder Dojo sea publicada en tales espacios digitales y así favorecer la visibilidad de la iniciativa: facilitando que más personas lo conozcan y se unan a la comunidad.

**¿Existe un seguro de responsabilidad civil?**
Es recomendable disponer de un seguro de responsabilidad civil en actividades abiertas a cualquier público, pero especialmente cuando las personas participantes son niños y niñas o jóvenes menores de edad. Por lo general, los espacios que hemos mencionado disponen de esta garantía y podremos acogernos a ella. Sin embargo, no está de más preguntar si existe, si podemos asociar nuestra actividad a dicho seguro y tener claros estos aspectos legales básicos.

Bien, ¿ya tenemos un espacio? Entonces estamos listos para empezar a difundir nuestro Coder Dojo y a invitar a más personas a unirse a nuestra comunidad.

![](images/coderDojo-foto-de-grupo.jpg)

## 3. Difundir la idea
En esta etapa inicial, hemos empezado con un equipo motor que ya ha empezado a asumir algunas responsabilidades y a realizar algunas tareas juntos: visitar un Coder Dojo para conocer cómo funciona y cómo se organiza, conseguir un espacio, y ahora toca empezar a construir la comunidad.

### Propósito
La difusión inicial del Coder Dojo tiene el propósito de dar a conocer la actividad que vamos a realizar e invitar a más personas a participar:

- Mentores y mentoras voluntarios/as: son las personas que tienen experiencia y/o conocimientos de programación, educación y/o dinamización de actividades con jóvenes. Podemos pensar en familiares (padres, madres, hermanos/as...), maestros y maestras, vecinos y vecinas, amigos y amigas o también otras personas que todavía no conocemos y que se unirán al recibir nuestra convocatoria.
- Chicos y chicas con ganas de aprender: son jóvenes con inquietudes e interesados en la tecnología, que quieren formar parte de una comunidad y experimentar otras formas de divertirse. Pueden ser compañeros y compañeras de clase o de otras actividades, vecinos y vecinas, amigos y amigas.

Fíjate que damos importancia a conformar una comunidad mixta, donde chicos y chicas aprenden juntos con el apoyo de mentores y mentoras. Una comunidad mixta tiene un gran potencial, ya que de nuestra diversidad surgirán tremendas oportunidades de aprendizaje, diálogos y experiencias.

Frecuentemente, los Coder Dojo realizan la difusión en dos convocatorias, es decir en dos momentos:

- Primero, una convocatoria para mentores y mentoras, que nos permite ampliar el grupo motor y empezar a poner ideas en común y planificar las sesiones.
- Segundo, con el equipo motor ampliado y mayor potencial de difusión, se pone en marcha una convocatoria para participantes, que se incorporarán a partir de la primera sesión o inauguración del Coder Dojo.

### Preparación

Nuestra primera tarea en esta primera campaña de difusión es consensuar y concretar la información que queremos transmitir. Para ello necesitaremos:

- Un título de la actividad: puede ser sencillamente Coder Dojo, aunque también podemos añadir el nombre de nuestro barrio, por ejemplo.
- Una breve descripción de la actividad en no más de un párrafo o una frase de invitación a la acción, como puede ser: ¿Quieres aprender a programar? ¡Únete a la primera comunidad de Coder Dojo en nuestro barrio!
- Lugar y fecha de la convocatoria: se refiere al espacio que ya hemos acordado y el horario y calendario que también hemos previsto en el paso anterior. Por ejemplo, Los sábados de 17h a 18.30h. A partir del 1 de octubre.
- Modo de inscripción: podemos indicar un teléfono de contacto, un correo electrónico o un formulario web donde se inscriban indicando si son mentores/as o participantes, su nombre y un teléfono o e-mail de contacto. Es recomendable que el equipo motor genere un correo electrónico específico para Coder Dojo, al que cualquiera en el equipo pueda acceder y gestionar, y que ofrezca un contacto común en lugar de uno personal. También podemos anotar una fecha límite de inscripción, para ver con tiempo cuántas personas se han apuntado.

Una vez tenemos claro todo lo anterior, podemos empezar a crear el elemento más básico para la difusión: un cartel en formato digital o papel. El cartel incluye alguna imagen, como puede ser el logo distintivo de Coder Dojo, así como texto con la información necesaria. Si creamos el cartel con una herramienta digital, como por ejemplo GIMP, necesitaremos también imprimir varias copias, y si lo creamos a mano de manera más artesanal, podemos escanearlo para enviarlo digitalmente y hacer copias para colocarlo en lugares estratégicos.

![](images/CoderDojo-logo-raspberry-pi.jpg)

### Medios
¿Dónde vamos a mover el cartel? O, dicho de otro modo, ¿dónde están las personas a las que queremos invitar? Para empezar, podemos abordar dos vías:

**Colocar carteles en lugares estratégicos del barrio**
Para llegar a gente que no usa habitualmente las redes sociales, pondremos el cartel que hemos diseñado en el propio espacio donde realizaremos el Coder Dojo, en el centro cultural, en la biblioteca, en los colegios e institutos, en el mercado, en locales de asociaciones, etc. Recuerda que antes de colocar un cartel es recomendable pedir permiso y preguntar por el lugar más indicado para colocarlo, ya que hay lugares que disponen de un tablón informativo.

Ir a colocar carteles es también una oportunidad de dar a conocer Coder Dojo a diversas instituciones, organizaciones y personas: ¡no dejes pasar la oportunidad de explicar en qué consiste todo esto!

**Compartir nuestro cartel en la red**
Podemos empezar por nuestro entorno cercano: familiares y amigos. Lo que más funciona es el boca oreja. En este sentido, podemos compartir nuestro cartel con nuestros contactos en las redes sociales o por correo electrónico. También podemos recopilar algunos contactos de instituciones o colectivos que nos ayuden a difundir la actividad, como por ejemplo AMPAs, centros juveniles, asociaciones del barrio u otros Coder Dojo cercanos.

Recuerda que es importante hacer un buen uso de los contactos. Por ejemplo, cuando enviamos un correo electrónico de difusión masivo (con muchos destinatarios), las direcciones de correo deben ir siempre en copia oculta para respetar la privacidad de estas personas o entidades.

Igual que es recomendable crear una cuenta de correo específica de nuestro Coder Dojo para enviar correos, cuando nos planteamos llegar a más personas podemos considerar la posibilidad de crear un perfil específico de Coder Dojo en redes sociales como puede ser Twitter o Instagram, o en una plataforma de micro-blogging como Tumblr. Estos perfiles nos serán útiles más adelante para comunicar lo que hacemos, dar visibilidad y compartir la experiencia de la comunidad.

## 4. Empezar nuestro Coder Dojo
¡Ya somos más! ¿Cuántas personas se han apuntado? ¿Hay un mínimo para empezar? Es importante que no nos agobiemos por la cantidad de personas con las que contamos en el grupo. Es mejor ser pocas pero con los objetivos claros que muchas y desorientadas. Nuestra recomendación es que aunque seamos un grupo de gente reducido, si estamos implicados en el proyecto, lo mejor es ponerlo a caminar.

### Planificar

El equipo motor, ya ampliado con la participación de mentores y mentoras, suele realizar una primera reunión para empezar a planificar las sesiones. Si es nuestra primera vez, aprenderemos sobre todo en la práctica. Pero en todo caso es recomendable dedicar un momento, una reunión previa, a pensar juntos una planificación:

- por dónde vamos a empezar,
- qué metodologías vamos a utilizar,
- cómo distribuimos el tiempo y el espacio en las sesiones,
- qué momentos dedicaremos a hacer, a pensar, a jugar, a reflexionar y dialogar.

No se trata aquí de llegar al máximo detalle, sino de empezar a imaginar juntos y poner en común unas bases que nos permitan orientarnos en los primeros pasos. Toda planificación contempla unos objetivos. En nuestro caso, hablamos de objetivos de aprendizaje de la programación, pero también de otros aspectos importantes en educación: convivencia, aprender a colaborar y cooperar, principios éticos en diseño y narrativa, creatividad, abordar temáticas sociales, etc. Estos objetivos han de ser consensuados por el equipo motor ampliado, y abiertos a revisión posterior con base en la experiencia.

La base temporal nos ayuda a concretar. Al planificar podemos ir de lo más grande a lo más pequeño:

**Un año**
Aproximadamente un año de Coder Dojo equivale a un curso escolar. Podemos igualmente anotar ideas para desarrollar en cada uno de los 3 trimestres del curso. Por ejemplo:

- en el primer trimestre, habrá buena parte de iniciación e introducción a conceptos, aprender a crear historias, pequeñas pruebas (por ejemplo, con animaciones, juegos sencillos, etc.);
- en el segundo trimestre, podemos empezar un primer proyecto poniendo en común inquietudes o motivaciones personales;
- en el tercer trimestre, podremos abordar proyectos más elaborados sobre alguna temática relevante de nuestro entorno (por ejemplo, sobre nuestro barrio o sobre el medioambiente).

**Un trimestre**

A lo largo de un trimestre, dispondremos de unas 10 sesiones de Coder Dojo. Un trimestre es un pequeño ciclo:

- empieza con una sesión de arranque en la que habrá una presentación, un primer encuentro, ayudaremos a que se dé una lluvia de ideas, estableceremos un diálogo y haremos una puesta en común sobre lo que queremos hacer;
- y termina también con una sesión de cierre donde podremos presentar el trabajo que hemos realizado, reflexionar y evaluar cómo ha funcionado todo, y también celebrar los avances y logros conseguidos.

Entre una y otra, iremos organizando sesiones de trabajo puntuales o agrupadas para llevar a cabo pequeños proyectos. Empezaremos planificando qué vamos a hacer y cómo lo haremos en las primeras sesiones. Más adelante ofrecemos un breve esquema que os puede servir de guía.

![](images/coderdojo-medialabprado17.jpg)

### Inaugurar
La primera sesión de todo Coder Dojo es una sesión muy especial. Es un momento para empezar a conocernos y crear un ambiente agradable, relajado y de confianza para el tipo de experiencia de aprendizaje que deseamos. Dedicaremos el tiempo necesario a presentarnos, escuchar, conocer y compartir inquietudes, expectativas y experiencias previas.

Podemos plantear diversos juegos o dinámicas para presentarnos. Los mentores y mentoras con experiencia en este tipo de dinámicas son clave para generar un buen ambiente desde el principio y mantenerlo vivo siempre.

También es el momento de explicar en detalle en qué consiste un Coder Dojo y qué principios o valores queremos promover, qué es una comunidad de aprendizaje, qué es la programación por bloques y qué herramientas vamos a utilizar. La inauguración es también una celebración. Fíjate en todo lo que ya habéis hecho para llegar hasta aquí. Ahora empezamos con mucha ilusión nuestro camino de aprendizaje juntos... ¡Comparte la alegría!

### Hacer
¿Cómo haremos una sesión en Coder Dojo? Una sesión es como un ritual: cada actividad tiene su momento y su espacio. Esta tabla nos puede servir de orientación para estructurar las sesiones de manera similar cada día y no olvidar las partes más importantes:

![](images/coderdojo-tabla-de-agenda.png)

Es importante recordar que los y las protagonistas en Coder Dojo son las personas participantes, los jóvenes en este caso. A veces, no es fácil para mentores y mentoras acompañar, motivar, provocar preguntas para que los jóvenes protagonicen su aprendizaje en lugar de simplemente decir lo que hay que hacer o cómo hacerlo. Es por eso que también mentores y mentoras van aprendiendo paso a paso su papel: dinamizar el aprendizaje cooperativo al tiempo que dar autonomía para explorar, probar y equivocarnos.

¡Aquí está el mayor aprendizaje de Coder Dojo!

![](images/coderdojo-medialabprado-unplugged2.jpg)

### Celebrar
*«La alegría de ver y entender es el más perfecto don de la naturaleza» Albert Einstein*

**En Coder Dojo hay momentos de intenso trabajo, de superar las dificultades, de ir paso a paso resolviendo retos, de aprender a hacer cosas juntos, dialogar... y todo esto, aunque es apasionante, no siempre es fácil. Es por eso que cada avance que vamos viendo en nuestro proceso colectivo es un motivo de celebración. En cada sesión podemos dedicar un momento específico para ello: por ejemplo, presentando las historias que hemos creado o los diseños que hemos preparado antes de programarlos, o disfrutando de ver nuestros programas en acción al final de la sesión.**

Pero también es fundamental destinar algunos días concretos a celebrar: un buen momento podría ser la última sesión del trimestre, donde además de presentar los proyectos que hemos realizado, ver los resultados y compartir nuestra experiencia durante su realización, podemos celebrarlo, preparar una merienda e invitar a nuestras familias y amigos.

## 5. Conectar con más personas
¿Cómo traducimos en la práctica los ideales de conocimiento abierto y cultura colaborativa? En nuestro proceso de aprendizaje en Coder Dojo nos serán muy útiles los conocimientos y la experiencia que otros han compartido en la red a través de guías, tutoriales, blogs. Así también, si documentamos nuestros avances y los compartimos en un blog o redes sociales, podrán servir a otros, e incluso a nosotros mismos más adelante para mejorar.

**Cuál es nuestra comunidad**
- Todas las personas involucradas en nuestro Coder Dojo: participantes y familias, mentores y mentoras.
- Nuestro barrio: las personas y entidades vinculadas al espacio en el que realizamos el Coder Dojo, así como otras iniciativas juveniles, asociaciones o instituciones con las que hemos establecido relación, y con las que es interesante realizar intercambios de vivencias.
- La comunidad internacional de Coder Dojo: que cuenta con otras experiencias similares que suceden en distintos lugares del mundo.

**Cómo compartir nuestra experiencia**

- Invitando a familiares, amigos y conocidos, así como a otros colectivos o entidades del barrio a venir a algunas sesiones, y especialmente, a las sesiones de presentación y celebración.
- Dando charlas divulgativas a otros grupos o entidades educativas (por ejemplo, en colegios e institutos), tecnológicas, de ámbito juvenil, etc.
- Participando en jornadas o eventos que organizan otros colectivos o entidades en nuestro espacio o en nuestro barrio.
- Documentando nuestra experiencia online con artículos de blog, reflexiones, fotos o pequeños vídeos en las redes. Un ejemplo interesante es elaborar una bitácora, es decir, un cuaderno de viaje en el que vamos anotando los pasos que vamos dando y las reflexiones y aprendizajes que surgen en cada sesión.
- Encontrando y conectando con otros Dojos u otras experiencias educativas de aprendizaje de la programación a través de redes sociales. En el sitio web de Coder Dojo podemos registrar nuestro nuevo grupo, encontrar otros cercanos y participar en los foros. De ese modo, quien esté buscando unirse a un Coder Dojo en nuestro barrio o ciudad, ¡nos encontrará ahí!

![](images/coderdojo-medialabprado-merienda2.jpg)

**Qué nos puede aportar**
- Difundir información sobre nuestro grupo nos ayuda a aprender no sólo de lo que pasa en él, sino de otras experiencias y perspectivas.
- Dar a conocer el nuestro para que más personas se unan y lo apoyen.
- Documentar lo que hacemos es una oportunidad para revisar y mejorar.

## 6. Evaluar para mejorar
¿Por qué evaluar? ¿No hemos dicho que en Coder Dojo no hay exámenes? Muy cierto. Sin embargo, no basta con hacer cosas, necesitamos también reflexionar sobre lo que hacemos y pensar cómo podemos hacerlo mejor. Aquí es tan importante prestar atención a los resultados (la animación o el videojuego que hemos creado) como a los procesos, es decir, a cómo nos organizamos, a qué pasos hemos dado, a cómo dialogamos y aprendemos. Una comunidad de aprendizaje avanza cuando es capaz de combinar la acción y la reflexión.

Al igual que en la planificación, nos ayuda ir por partes:

- En cada sesión, podemos hacer una pequeña revisión en común todo el grupo al cierre de la sesión. Después también podemos reservar unos minutos para una breve reflexión entre mentores y mentoras.
- Al finalizar cada trimestre, podemos revisar de manera más global cómo ha funcionado la comunidad durante todo el periodo en una breve reunión.
- Al cierre del año tendremos toda la información para valorar el proceso de todo el tiempo en su conjunto.

Hay diversas formas en que podemos abordar la revisión y evaluación. A modo orientativo nos puede servir este sencillo esquema, que puede utilizarse como índice en las reuniones de evaluación:

- Ver: lo que ha sucedido y anotar lo que ha sido beneficioso y lo que ha generado dificultades o impedimentos.
- Analizar: preguntarnos cómo ha sucedido lo que hemos anotado en el punto anterior y por qué ha sido así.
- Actuar: proponer otras formas de hacer algo en tales situaciones y ponerlas en práctica en las siguientes sesiones.

Siempre es posible mejorar algún aspecto, tanto en las cuestiones técnicas como especialmente en las cuestiones no técnicas: comunicación, convivencia, organización, metodología e implicación con la comunidad. Solo así, el próximo año será aún mejor. ¿No estás deseando YA armar un Coder Dojo?
