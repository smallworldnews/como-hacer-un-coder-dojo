# Consejos

- Escuchar y dialogar: es lo básico cuando compartimos una experiencia de aprendizaje.
- Conseguir un grupo mixto donde chicas y chicos trabajamos juntos: mayor diversidad dará como resultado proyectos más ricos e interesantes.
- Marcar vuestro propio ritmo, poco a poco: lo importante es ir dando pasos, nada grande se consigue de un golpe.
- Cooperar para aprender juntos en lugar de competir: todos tenemos algo que aportar y algo que recibir para mejorar; el propósito es hacerlo lo mejor posible con ayuda de los compañeros y compañeras.
- Aprender de los errores: tanto al programar como en todo lo demás.
- Dar respuesta a problemas e historias reales: poner en marcha la imaginación para transformar la realidad.
- Respetar los momentos y espacios para cada actividad: buscando orden, armonía y un ambiente respetuoso para la convivencia.
- Mantener el compromiso: es posible hacer grandes cosas sólo cuando le dedicamos tiempo y esfuerzo de manera continuada.
- Comunicar lo que hacemos: ya que estamos contribuyendo al conocimiento abierto con nuestros aprendizajes, reflexiones y proyectos.
