#Materiales

## Espacio
- Una sala con mesas y sillas móviles, para programar individualmente o en grupo.
- Zonas para movernos, jugar y pensar juntos.
- Conexión WiFi o cableada a internet.

## Material técnico
- Ordenadores o portátiles con cargador.
- Proyector y altavoces.
- Regletas de enchufes.

## Software
- Scratch o AppInventor instalado en el ordenador.
- Herramientas unplugged
- Flujogramas.
- Storyboard.

## Material de papelería.
- Rotuladores, folios, tijeras, pegamento, papel continuo, cinta adhesiva y cinta carrocera.
- Servicios
- Plataforma online de Scratch donde subir nuestros proyectos.
- Micro-blog o redes sociales donde comunicamos lo que hacemos (Instagram, Tumblr, WordPress, Blogger).

## Electrónica (opcional)
- Arduino.
- Raspberry Pi
